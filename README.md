# Tekton Resources

A collection of Tekton resources, such as pipelines, tasks and so on.

## Project Structure

* `images`: container images used in tasks

## License

[Apache-2.0](./LICENSE)
